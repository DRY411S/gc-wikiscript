"""
	Build the JSON files required by the wiki gamedata.js
	The master source file is gamedata.json that ships with the game
	3 JSON files are required by the wiki gamedata.js
		materials.json		- Cases,
								Modules,
								Components,
								Base Materials,
								special, and
								Campaign specific items
		techs.json			- The 'Research Tree'
		equipment.json		- Tables, Machines, Decoratives, etc
		
	***
	
	This is a work in progress.
	
	Desired end state is to generate the 3 files from gamedata.json
	
	***
	
	This version
		- Generates equipment.json wholly from gamedata.json
		
	***
"""

# Required json modules
from cgi import print_arguments
import json
import datetime
from struct import pack
from turtle import exitonclick

# Load the source data
f = open("../gamedb/gamedata/gamedata.json", "rt")
gamedata = json.loads(f.read())
f.close()

"""
	Scope of Equipment

	Crafting Tables
	Crafting Machines
	Research & Analysis Tables and Stations
	Storage
	Conveyors
	Decoratives
		Ground
		Monument
		Plant
		Wall
		Office
		Seaonal
	Miscellaneous
	Null
"""
"""
	Source of equipment type is "BuildableCategories"
	Includes CAT_AdvancedCrafting which I did not see in the original equipment.json
	"CrafterProperties" appears to have data about tables and machines
	"BuildableProperties" looks important
	"ObjectTypes" are the names
	"AssemblyProperties" is information about AssemblyTables
	"ConveyorProperties"
	"SpecialFlagsProperties" has some fields I think
	"ResearchAndDevelopmentProperties" things about research and analysis
	"ObjectLookProperties" could be the decoratives

	ObjectTypes are every piece of Equipment Type
	Then BuildableProperties gives the category based on Equipmet Type
	The BuildableCategories returns the Name of the Equipment Category
	The Name will determine which of the other properties we need to use
"""
Equipment =	{
				"version" : "1.0",
				"equipment" : []
			}

for ObjectType in gamedata["ObjectTypes"]:
	if ObjectType in gamedata["BuildableProperties"].keys():
		BuildableProperty = gamedata["BuildableProperties"][ObjectType]
		BuildableCategory = gamedata["BuildableCategories"][str(BuildableProperty["Category"])]
		CategoryName = BuildableCategory["Name"]
		"""
			What we do now depends on the CategoryName
			But some is common to every Category
		"""
		"""
		equipment_category = CategoryName
		can_be_built_outdoors = BuildableProperty["CanBeBuiltOutdoors"]
		order_in_category = BuildableProperty["OrderInCategory"]
		equipment_cost = BuildableProperty["BuildCost"]
		materials = BuildableProperty["NeededMaterials"]
		icon_sprite = gamedata["IconProperties"][ObjectType]["TextSpriteAssetID"]
		# assembly_properties
		equip_id = int(ObjectType)
		icon_id = gamedata["ObjectTypes"][ObjectType]["Name"]
		# crafter_properties
		# inventory_properties (Decoration)
		# research_properties
		loca_string = icon_id
		"""
		ThisObject =	{
							"buildable_properties": [
														{
															"equipment_category": CategoryName, 
															"can_be_built_outdoors": BuildableProperty["CanBeBuiltOutdoors"], 
															"order_in_category": BuildableProperty["OrderInCategory"], 
															"equipment_cost": BuildableProperty["BuildCost"], 
															"materials": BuildableProperty["NeededMaterials"]
														}
													], 
							"icon_sprite": gamedata["IconProperties"][ObjectType]["TextSpriteAssetID"], 
							"assembly_properties": [], 
							"equip_id": int(ObjectType), 
							"icon_id": gamedata["ObjectTypes"][ObjectType]["Name"], 
							"crafter_properties": [], 
							"inventory_properties": [], 
							"research_properties": [], 
							"loca_string": gamedata["ObjectTypes"][ObjectType]["Name"]
						}
		if "Decoration" in CategoryName:
			if ObjectType in gamedata["InventoryProperties"].keys():
				ThisObject["inventory_properties"].append({"inventory_slots": gamedata["InventoryProperties"][ObjectType]["NumberOfSlots"]})
		elif CategoryName == "CAT_Assembly":
			"""
				Need to add what this assembly table can be used for
				Source is AssemblyProperties
			"""
			CraftingList = []
			if str(ObjectType) in gamedata["AssemblyProperties"].keys():
				ThisAssembler = gamedata["AssemblyProperties"][str(ObjectType)]["ProductTypes"]
				for ThisProduct in ThisAssembler:
					icon_id = gamedata["ProductTypes"][str(ThisProduct["ProductTypeID"])]["Name"]
					icon_id = icon_id.split("_")
					ListItem = {	"base_duration": ThisProduct["Duration"] / gamedata["TuningValues"]["SecondsPerDay"],
									"icon_sprite": "icons_types", 
									"icon_id": icon_id[1], # Not in gamedata.json, needs to be computed, could strip 5 'type_' chars from the Name
									"product_id": ThisProduct["ProductTypeID"], 
									"loca_string": gamedata["ProductTypes"][str(ThisProduct["ProductTypeID"])]["Name"]
								}
					CraftingList.append(ListItem)
				ThisObject["crafter_properties"].append({"crafting_list" : CraftingList})
		elif CategoryName == "CAT_Research":
			"""
				Need to add what this research/analysis table can do
				Source is "ResearchAndDevelopmentProperties"
				Output depends on whether this is an Analysis or Research table
			"""
			research_property =	{	"research_time": gamedata["ResearchAndDevelopmentProperties"][ObjectType]["Speed"], 
									"can_do_analysis": gamedata["ResearchAndDevelopmentProperties"][ObjectType]["CanDoResearch"], 
									"can_do_research": gamedata["ResearchAndDevelopmentProperties"][ObjectType]["CanDoDevelopment"]
								}
			ThisObject["research_properties"].append(research_property)
		elif CategoryName == "CAT_Conveyor":
			# materials needs updating. if it already has some value from above
			if len(ThisObject["buildable_properties"][0]["materials"]) == 1:
				ListMaterial =	{
									"icon_sprite": "icons_components",
									"icon_id": "itm_conveyor_part",
									"loca_string": "itm_conveyor_part",
									"material_id": ThisObject["buildable_properties"][0]["materials"][0]["MaterialID"],
									"material_amount": ThisObject["buildable_properties"][0]["materials"][0]["Amount"]
								}
				ThisObject["buildable_properties"][0]["materials"][0].clear()
				ThisObject["buildable_properties"][0]["materials"][0].update(ListMaterial)
			# inventory_properties needs updating
			if ObjectType in gamedata["InventoryProperties"].keys():
				ThisObject["inventory_properties"].append({"inventory_slots": gamedata["InventoryProperties"][ObjectType]["NumberOfSlots"]})
			# exit()
		elif CategoryName == "CAT_BasicCrafting" or CategoryName == "CAT_Machines":
			if CategoryName == "CAT_Machines":
				ListMaterial =	{
									"icon_sprite": "icons_components",
									"icon_id": "itm_machine_part",
									"loca_string": "itm_machine_part",
									"material_id": ThisObject["buildable_properties"][0]["materials"][0]["MaterialID"],
									"material_amount": ThisObject["buildable_properties"][0]["materials"][0]["Amount"]
								}
				ThisObject["buildable_properties"][0]["materials"][0].clear()
				ThisObject["buildable_properties"][0]["materials"][0].update(ListMaterial)
			CraftingList = []
			ThisCrafter = gamedata["CrafterProperties"][ObjectType]["PossibleModules"]
			for ThisProduct in ThisCrafter:
				icon_sprite = gamedata["Modules"][str(ThisProduct["ModuleID"])]["IconAssetID"]
				icon_sprite = icon_sprite.split("/")
				ListItem = {	"icon_id": gamedata["Modules"][str(ThisProduct["ModuleID"])]["Name"],
								"loca_string": gamedata["Modules"][str(ThisProduct["ModuleID"])]["Name"],
								"icon_sprite": icon_sprite[0], # need to strip the /Name from the end
								"craft_batch": ThisProduct["BatchSize"],
								"module_id": ThisProduct["ModuleID"],
								"craft_duration": ThisProduct["Duration"] / gamedata["TuningValues"]["SecondsPerDay"]
							}
				CraftingList.append(ListItem)
			CrafterProperties = {	"crafting_list": CraftingList,
									"automatic": gamedata["CrafterProperties"][ObjectType]["Automatic"]
								}
			ThisObject["crafter_properties"].append(CrafterProperties)
		elif CategoryName == "CAT_Misc":
			# Only one legacy item. Does not need changing at all
			x = 0
		elif CategoryName == "CAT_Storage":
			if ObjectType in gamedata["InventoryProperties"].keys():
				ThisObject["inventory_properties"].append({"inventory_slots": gamedata["InventoryProperties"][ObjectType]["NumberOfSlots"]})
		else:
			"""
				*******************************************************************

				Crash out here if we find a CategoryName that is new and we have
				no code to handle it

				*******************************************************************
			"""
			print("*** UNKNOWN CATEGORY ***")
			print(CategoryName)
			exit()

		Equipment["equipment"].append(ThisObject)

	else:
		# G_Box, bld_buyzone_tier01, bld_sellzone_tier01, and playerChar ObjectTypes
		# all do not appear in "BuildableProperties"
		# bld_buyzone_tier01, bld_sellzone_tier01 appear as 'null' in the original equipment json
		# but these are legacy and no longer needed
		# I will have to hardcode something if they are required
		# Assumption is that they are not
		pass

# Write the equipment.json
# to avoid corrupting the original file
# make a backup

f = open("../gamedb/gamedata/equipment.json", "rt")
backup = (f.read())
f.close()
# Create the backup
stamp = datetime.datetime.now()
stampstr = stamp.strftime("../archive/equipment%Y%m%d-%H%M%S.json")
f = open(stampstr, "xt")
f.write(backup)
f.close()

# Write the new file

j = json.dumps(Equipment, indent = 4)
f = open("../gamedb/gamedata/equipment.json", "w")
f.write(j)
f.close()